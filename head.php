<?php
$hash = basename($_SERVER['QUERY_STRING']);
$filsName = basename($_SERVER['PHP_SELF']);
if($hash!='')
	$filsName .= "?".$hash;
$base_url = "//".$_SERVER['SERVER_NAME'].str_replace($filsName,'',$_SERVER['REQUEST_URI']);
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
<link rel="icon" type="image/png" href="images/favicon.ico">
<link rel="stylesheet" href="<?php echo $base_url;?>css/style.css"/>
<link rel="stylesheet" href="<?php echo $base_url;?>css/bootstrap.min.css"/>
<link rel="stylesheet" href="<?php echo $base_url;?>css/bootstrap-theme.min.css"/>
<link rel="stylesheet" href="<?php echo $base_url;?>css/font-awesome.min.css"/>
<link rel="stylesheet" href="<?php echo $base_url;?>css/style.css"/>
<link rel="stylesheet" href="<?php echo $base_url;?>css/ui-blue/jquery-ui.min.css"/>
<script src='<?php echo $base_url;?>js/jquery-1.11.1.min.js'></script>
<script src='<?php echo $base_url;?>js/jquery-ui.min.js'>
<script src='<?php echo $base_url;?>js/bootstrap.min.js'></script>
<script src='<?php echo $base_url;?>js/retina-1.1.0.min.js'></script>
<script src='<?php echo $base_url;?>js/home.js'></script>
<script>
    <!--
    var base_url = '<?php echo $base_url;?>';
    -->
</script>