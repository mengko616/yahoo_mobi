/**
 * @author 蔡孟珂
 */
$(document).ready(function(){
	
	//如果為retina
	if (window.devicePixelRatio >= 2)
	{
		//改變圖檔
		if($("div.logo").length == 1)
		{
			var retinaUrl = $("div.logo").css('background-image').replace('url(','').replace(')','');
			var subname = retinaUrl.substr((retinaUrl.length-4),4);
			retinaUrl = retinaUrl.replace(subname,'@2x'+subname);
			$("div.logo").css('background-image','url('+retinaUrl+')');
		}
	}
	
	//判別是否已經看過該頁
	$("ul.menu a").on('click',function(){
		if($(this).attr('class') == 'active')
		{
			return false;
		}
		else
		{
			var notlook = true;
			$.ajax({
				type : "POST",
				async: false,
				url : base_url+"check_page_has_view.php",
				data : {
					p: $(this).attr('rel')
				},
				dataType : 'json'
			}).done(function(data) {
				if (data.result)
				{
					if(data.done)
					{
						$("#dialog").html('已看完6個新聞類目，請等候實驗人員，進行下一個操作項目。');
						$("#dialog").dialog({
							resizable : false,
							height : 160,
							width : 250,
							modal : true,
							buttons : {
								'確定' : function() {
									$(this).dialog("close");
								}
							}
						});
						//alert('已看完6個新聞類目，請等候實驗人員，進行下一個操作項目。');
					}
					else
					{
						$("#dialog").html(data.msg);
						$("#dialog").dialog({
							resizable : false,
							height : 160,
							width : 250,
							modal : true,
							buttons : {
								'確定' : function() {
									$(this).dialog("close");
								}
							}
						});
						//回傳true就是有看過 
						//alert(data.msg);
					}
					notlook = false;
				}
			}).fail(function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR.responseText);
			});
			return notlook;
		}
	});
	
	
	
	//清除觀看記錄
	$("a.clear_data").on('click',function(){
		$.ajax({
			type : "POST",
			url : base_url+"clear_session.php",
			dataType : 'json'
		}).done(function(data) {
			if(data.result)
			{
				alert(data.msg);
			}
		}).fail(function(jqXHR, textStatus, errorThrown) {
			console.log(jqXHR.responseText);
		});
	});
	
	//固定選單
	if($("div.menu_box").length == 1)
	{
		var fixH = $("div.header").innerHeight() + $("div.search_bar").innerHeight();
		var blankH = $("div.menu_box").innerHeight();
		$('body').on('touchmove',function(){
			if($('body').scrollTop()>=fixH)
			{
				//console.log(fixH);
				$("div.menu_box").addClass('menu-fixed');
				if($("div.blank").length == 0)
				{
					$("div.menu_box").parent().append("<div class='blank' style='display:block;height:"+blankH+"px;'></div>");
				}
			}
			else
			{
				$("div.menu_box").removeClass('menu-fixed');
				$("div.blank").remove();
			}
		});
	}
});