<!DOCTYPE html>
<html lang="zh-TW">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>行動首頁</title>
        <!-- 下方 include_once 不可移除 -->
        <?php include_once 'head.php';?>
    </head>

    <body>
        <div class='container'>
            <?php 
            include_once 'news_content.php';
            include_once 'news_header.php';
            $p = 0;
            $index = 0;
            if(isset($_GET['p']))
            {
                $p = $_GET['p'];
            }
            if(isset($_GET['i']))
            {
                $index = $_GET['i'];
            }
            //取得新聞
            $news = $news_array[$p][$index];
            ?>
            <div class='row news_content'>
                <div class='col-xs-12'>
                	<h1><?php echo $news['title'];?></h1>
                </div>
                <p class='form'><?php echo $news['form']?></p>
                <p class='time'><?php echo $news['time']?></p>
                <div class='content_text'>
                	<?php echo $news['content']?>
                </div>
            </div>
        </div>
    </body>
</html>