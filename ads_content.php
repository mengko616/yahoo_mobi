<?php
    /**
     * 修改說明：
     * 1.圖檔大小，寬最小需400px，高度不限。
     * 2.只要將對應的廣告圖檔，請放置在 images/ads/ 資料夾中。
     * 3.廣告圖修改，對下面的廣告img所指向的圖檔檔名進行置換即可。
     * 4.url所指向的是該廣告點擊後的網站網址。
     */

    //頭條的廣告
    $ad_array[0] = array(
        3=>array("img"=>"content_ANZ_01.jpg","content_img"=>"anz.png"),       //第4則
        9=>array("img"=>"content_BMW_02.jpg","content_img"=>"bmw.png"),   //第9則
        15=>array("img"=>"content_HTC_03.jpg","content_img"=>"htc_one.png")        //第14則
    );
    
    //娛樂的廣告
    $ad_array[1] = array(
        3=>array("img"=>"content_Hesung_04.jpg","content_img"=>"he_sung.png"),       //第4則
        9=>array("img"=>"content_extra_05.jpg","content_img"=>"extra.png"),   //第9則
        15=>array("img"=>"content_DHC_06.jpg","content_img"=>"dhc.png")    //第14則
    );
    
    //運動的廣告
    $ad_array[2] = array(
        //第4則
        3=>array(
            "img"=>"big_03.png",
            "content_img"=>"aia.png",
            "title"=>'標題',
            "abstract"=>"摘要"
        ),      
        //第9則
        9=>array(
            "img"=>"big_02.png",
            "content_img"=>"smart.png",
            "title"=>'標題',
            "abstract"=>"摘要"
        ),  
        //第14則        
        15=>array(
            "img"=>"big_04.png",
            "content_img"=>"lg.png",
            "title"=>'標題',
            "abstract"=>"摘要"
        )           
    );
    
    //政經的廣告
    $ad_array[3] = array(
        //第4則
        3=>array(
            "img"=>"big_01.png",
            "content_img"=>"airwave.png",
            "title"=>'標題',
            "abstract"=>"摘要"
        ),      
        //第9則
        9=>array(
            "img"=>"big_05.png",
            "content_img"=>"UNT.png",
            "title"=>'標題',
            "abstract"=>"摘要"
        ),  
        //第14則        
        15=>array(
            "img"=>"big_06.png",
            "content_img"=>"Desperados.png",
            "title"=>'標題',
            "abstract"=>"摘要"
        )
    );
    
    //社會的廣告
    $ad_array[4] = array(
        3=>array("img"=>"banner_esun.jpg","content_img"=>"esun.png"),        //第4則
        9=>array("img"=>"banner_toyota.jpg","content_img"=>"toyota.png"),   //第9則
        15=>array("img"=>"banner_acer.jpg","content_img"=>"acer.png")    //第14則
    );
    
    //新奇的廣告
    $ad_array[5] = array(
        3=>array("img"=>"banner_Rainbow.jpg","content_img"=>"rainbow.png"),        //第4則
        9=>array("img"=>"banner_cosmed.png","content_img"=>"cosmed.png"),   //第9則
        15=>array("img"=>"banner_johnnie_walker.jpg","content_img"=>"johnnie_walker.png")    //第14則
    );
?>