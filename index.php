<!DOCTYPE html>
<html lang="zh-TW">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>行動首頁</title>
		<!-- 下方 include_once 不可移除 -->
        <?php include_once 'head.php';?>
	</head>

	<body>
	    <div class='home_header'>
	        Emerging Mobile Ads Research 
	    </div>
	    <img src='images/yahoo_logo.png' class='yahoo_logo'>
		<div class='container text-center'>
			<div class='row'>
			    <p class='start_btn'>
			        <a href='yahoo.php' class='btn btn-default btn-lg'>Yahoo!行動首頁</a>
			    </p>
			    <p class='clear_btn'>
			        <a href='javascript:void(0);' class='btn btn-danger clear_data'>清除記錄</a>
			    </p>
            </div>
		</div>
		<footer class='text-right'>
		    <img src='images/cclab_logo.png'>
		</footer>
	</body>
</html>