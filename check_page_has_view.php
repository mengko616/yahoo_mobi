<?php
    session_start();
    $p = $_POST['p'];
    $json = array('result'=>FALSE,'msg'=>'尚未觀看','done'=>false);
    if(in_array($p,$_SESSION['has_view_page']))
    {
         $json = array('result'=>true,'msg'=>'此新聞類目已經看過，請點選其他新聞類目','done'=>false);
    }
	if(count($_SESSION['has_view_page']) == 6)
		$json['done']=true;
	
    header('Content-Type: application/json; charset=utf-8');
    echo json_encode($json);
?>