<?php
session_start();
//預設為 頭條 0
$p = 0;
if(isset($_GET['p']))
{
    $p = $_GET['p'];
}

if(isset($_SESSION['has_view_page']))
{   
    if(!in_array($p,$_SESSION['has_view_page']))
    {
         $_SESSION['has_view_page'][] = $p;
    }
}
else
{
    $_SESSION['has_view_page'][] = $p;
}
?>

<!DOCTYPE html>
<html lang="zh-TW">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>行動首頁</title> 
        <!-- 下方 include_once 不可移除 -->
        <?php
        include_once 'head.php';
        include_once 'news_content.php';
        include_once 'ads_content.php';
        ?>
        
    </head>

    <body>
        <div class='container'>
            <div class='row header'>
                <div class='div-left'>
                    <i class="fa fa-bars nav-icon"></i>
                    <div class='logo'></div>
                </div>
                <div class='div-right'>
                    <div class='header_icons'>
                        <i class='bid'></i>
                        拍賣
                    </div>
                    <div class='header_icons'>
                        <i class='shopping'></i>
                        購物
                    </div>
                    <div class='header_icons '>
                        <i class='store'></i>
                        商城
                    </div>
                    <div class='header_icons '>
                        <i class='arrow-icon'></i>
                    </div>
                </div>
            </div>
            <div class='row search_bar'>
                <div class='col-xs-10'>
                    <div class="input-group">
                        <input type="text" class="form-control " placeholder="請輸入關鍵字" disabled="disabled">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-white">
                                <span class='glyphicon glyphicon-search search_btn'></span>
                            </button> 
                        </span>
                    </div>
                </div>
                <div class='col-xs-2'>
                    <div class='header_icons'>
                        <i class='mail'></i>
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class='menu_box col-xs-12'>
                    <ul class='menu'>
                       <li><a href='?p=0' <?php echo ($p == 0)?"class='active'":'';?> rel='0'>頭條</a></li>
                       <li><a href='?p=1' <?php echo ($p == 1)?"class='active'":'';?> rel='1'>娛樂</a></li>
                       <li><a href='?p=2' <?php echo ($p == 2)?"class='active'":'';?> rel='2'>運動</a></li>
                       <li><a href='?p=3' <?php echo ($p == 3)?"class='active'":'';?> rel='3'>政經</a></li>
                       <li><a href='?p=4' <?php echo ($p == 4)?"class='active'":'';?> rel='4'>社會</a></li>
                       <li><a href='?p=5' <?php echo ($p == 5)?"class='active'":'';?> rel='5'>新奇</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <?php
        $isBig = array(2,3);
        $child_no_padding = '';
        if(in_array($p,$isBig))
        {
            $child_no_padding = ' no_padding';
        }
        ?>
        <ul class='news_list<?php echo $child_no_padding;?>'>
            <?php
            foreach($news_array[$p] as $key=>$row):
                if(in_array($p,$isBig))
                {
                    //為運動、政經
                    //在3,10,17項 顯示大廣告
                    if(in_array($key,array(3,9,15))):
                    ?>
                    <li <?php if($p == 0 || $p == 1):?>class='ad'<? endif;?>>
                        <a href='<?php echo "ad_show.php?p={$p}&i={$key}";?>' class='big_cover_news ad' style='background-image:url(<?php echo "images/ads/".$ad_array[$p][$key]['img'];?>);'>
                            <div>
                                <h3>
                                    <?php
                                    if(mb_strlen($ad_array[$p][$key]['title'], 'UTF-8') > 23)
                                    {
                                        echo mb_substr($ad_array[$p][$key]['title'],0,23,'UTF-8')."...";
                                    }   
                                    else 
                                    {
                                        echo $ad_array[$p][$key]['title'];
                                    }
                                    ?>
                                </h3>
                                <p>
                                    <?php
                                    $text = str_replace(array("\n",' '),'',strip_tags($ad_array[$p][$key]['abstract']));
                                    if(mb_strlen($text, 'UTF-8') > 50)
                                    {
                                        echo mb_substr($text,0,50,'UTF-8')."...";
                                    }   
                                    else 
                                    {
                                        echo strip_tags($ad_array[$p][$key]['abstract']);
                                    }
                                    ?>
                                </p>
                            </div>
                        </a>
                    </li>
                    <?php endif;?>
                    <li>
                        <a href='<?php echo "news_show.php?p={$p}&i={$key}";?>' class='big_cover_news' style='background-image:url(<?php echo "images/news/".$row['img'];?>);'>
                            <div>
                                <h3>
                                    <?php
                                    if(mb_strlen($row['title'], 'UTF-8') > 23)
                                    {
                                        echo mb_substr($row['title'],0,23,'UTF-8')."...";
                                    }   
                                    else 
                                    {
                                        echo $row['title'];
                                    }
                                    ?>
                                </h3>
                                <p>
                                    <?php
                                    $text = str_replace(array("\n",' '),'',strip_tags($row['content']));
                                    if(mb_strlen($text, 'UTF-8') > 50)
                                    {
                                        echo mb_substr($text,0,50,'UTF-8')."...";
                                    }   
                                    else 
                                    {
                                        echo strip_tags($row['content']);
                                    }
                                    ?>
                                </p>
                            </div>
                        </a>
                    </li>
                    <?php
                }
                else
                {
                    //在3,10,17項 顯示圖文，或橫幅廣告
                    if(in_array($key,array(3,9,15))):
                    ?>
                    <li <?php if($p == 0 || $p == 1):?>class='ad'<? endif;?>>
                        <a href='<?php echo "ad_show.php?p={$p}&i={$key}";?>'>
                            <img src='images/ads/<?php echo $ad_array[$p][$key]['img'];?>' class="ad">
                        </a>
                    </li>
                    <?php endif;
                    if($key == 0):
                    //為該分類第一筆
                    ?>
                    <li>
                        <a href='<?php echo "news_show.php?p={$p}&i={$key}";?>' class='cover_news' style='background-image:url(<?php echo "images/news/".$row['img'];?>);'>
                            <div>
                                <h3><?php echo $row['title'];?></h3>
                            </div>
                        </a>
                    </li>
                    <?php else:
                    //為其他筆數 
                    ?>
                    <li>
                        <a href='<?php echo "news_show.php?p={$p}&i={$key}";?>' class='other_news'>
                            <div class='list_thumb' style='background-image:url(<?php echo "images/news/".$row['img'];?>);'></div>
                            <div class='list_info'>
                                <h3><?php echo $row['title'];?></h3>
                                <p><?php echo $row['form'];?></p>   
                            </div>
                            <div class='clear_fix'></div>
                        </a>
                    </li>
                    <?php endif;
                    
                        
                }
            endforeach;
            ?>
        </ul>
        <div id='dialog'></div>
    </body>
</html>