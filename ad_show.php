<!DOCTYPE html>
<html lang="zh-TW">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>廣告</title>
        <!-- 下方 include_once 不可移除 -->
        <?php include_once 'head.php';?>
    </head>

    <body>
        <div class='container'>
            <?php 
            include_once 'ads_content.php';
            include_once 'ad_header.php';
            $p = 0;
            $index = 0;
            if(isset($_GET['p']))
            {
                $p = $_GET['p'];
            }
            if(isset($_GET['i']))
            {
                $index = $_GET['i'];
            }
            //取得廣告
            $ad = $ad_array[$p][$index];
            ?>
            <div class='row news_content'>
                <img src='<?php echo "images/ads/".$ad['content_img'];?>' class='ad'>
            </div>
        </div>
    </body>
</html>